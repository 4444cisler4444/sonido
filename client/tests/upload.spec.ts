import { test, expect } from '@playwright/test';

test.beforeEach(async ({ page }) => {
  await page.goto('http://localhost:5173/');

  // if is authenticated already, don't log in
  if (await page.isVisible('button[data-testid="real-user"]')) {
    return;
  }

  // login
  await page.getByText('Join').click();
  await page.getByText('Log In').nth(0).click();

  const email = await page.waitForSelector('input#email');
  await email.fill('foo2@bar2.com');
  await email.press('Enter');

  await expect(page.getByTestId('share-track')).toBeVisible();
});

test('has working functionality', async ({ page }) => {
  await page.getByTestId('share-track').click();
  await page.getByText('Upload Sound').click();
  await page.setInputFiles('input[type="file"]', 'tests/test.mp3');

  page.on('dialog', dialog => {
    expect(dialog.message()).toBeTruthy();

    return dialog.accept();
  });

  const uploadBtn = page.getByTestId('upload-btn');
  await uploadBtn.click();

  await new Promise(done => setTimeout(done, 1000 * 5));

  await expect(page.getByText('Comments')).toBeVisible();
  await expect(page.getByText('test.mp3').nth(0)).toBeVisible();
});
