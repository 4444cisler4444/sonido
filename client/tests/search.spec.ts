import { test, expect } from '@playwright/test';

test.beforeEach(async ({ page }) => {
  await page.goto('http://localhost:5173/');

  await page.getByTestId('search-btn').click();
});

test.afterEach(async ({ page }) => {
  await page.getByTestId('close-search').click();
});

test.describe('Search users functionality', () => {
  test('should allow me to search users', async ({ page }) => {
    const s = page.getByPlaceholder('Find tracks or artists');

    await s.fill("foo2");
    await s.press("Enter");
    await expect(page.getByTestId("found-user")).toContainText("foo2");
  });

  test('should allow me to search sound tracks', async ({ page }) => {
    const s = page.getByPlaceholder('Find tracks or artists');

    await s.fill("March-2");
    await s.press("Enter");
    const foundSound = page.getByTestId("found-sound").nth(0);
    await expect(foundSound).toContainText("March-2");
  });
});
