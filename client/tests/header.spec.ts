import { test, expect } from '@playwright/test';

test.beforeEach(async ({ page }) => {
  await page.goto('http://localhost:5173/');
});

test('has title', async ({ page }) => {
  await expect(page.getByTestId('home-link')).toBeVisible();
});

test('has working log in flow', async ({ page }) => {
  // logged in? log out.
  if (await page.isVisible('button[data-testid="real-user"]')) {
    await page.getByTestId('button[data-testid="real-user"]').click();
  }

  await page.getByTestId('join-share-btn').click();
  await page.getByText('Log In').nth(0).click();

  const email = await page.waitForSelector('input#email');
  await email.fill('foo2@bar2.com');
  await email.press('Enter');

  await expect(page.getByTestId('share-track')).toBeVisible();
});
