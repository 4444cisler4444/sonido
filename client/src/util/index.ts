import superagent from "superagent";
import { Options, toast } from "vue3-toastify";
import { createWebHistory, RouteRecordRaw, createRouter } from "vue-router";
import { ref } from "vue";

import AuthorPage from "../components/AuthorPage.vue";
import SoundPage from "../components/SoundPage.vue";
import Timeline from "../components/Timeline.vue";

export * from "./St8";

export const Paths = {
  home: "/",
  sounds: (soundId: string | number = ":soundId") => `/sounds/${soundId}`,
  users: (userId: string | number = ":userId") => `/u/${userId}`,
} as const;

export const routes: RouteRecordRaw[] = [
  { path: Paths.home, component: Timeline },
  { path: Paths.sounds(), component: SoundPage },
  { path: Paths.users(), component: AuthorPage },
];

export const router = createRouter({
  history: createWebHistory(),
  routes,
});

// Output: July 2
const formatter = new Intl.DateTimeFormat("en-US", {
  month: "long",
  day: "numeric",
});

export function format(timestamp: number) {
  const date = new Date(timestamp);
  return formatter.format(date);
}

export function snakeCase(r: Record<string, unknown>): Record<string, unknown> {
  const out: Record<string, unknown> = {};
  for (const [k, v] of Object.entries(r)) {
    out[k.replace(/([A-Z])/g, "_$1").toLowerCase()] = v;
  }
  return out;
}

export const notifyIds = ref<[string, string | number][]>([]);

export function notify(message: string, opts?: Options) {
  const isDarkTheme =
    window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches;

  const id = isDarkTheme ? toast.dark(message, opts) : toast.info(message, opts);

  notifyIds.value.push([message, id]);
}

export function notifyPromise<T>(p: Promise<T>, msg: string) {
  const isDarkTheme =
    window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches;

  return toast.promise(p, { pending: msg }, { theme: isDarkTheme ? "dark" : "auto" });
}

export function formatRelative(timestampMs: number): string {
  const date = new Date(timestampMs);
  const now = new Date();

  const diff = now.getTime() - date.getTime();

  // Constants for time calculations
  const ONE_MINUTE = 60 * 1000;
  const ONE_HOUR = 60 * ONE_MINUTE;
  const ONE_DAY = 24 * ONE_HOUR;

  if (diff < 0) {
    return "in the future"; // Handle future timestamps
  } else if (diff < ONE_MINUTE) {
    return "just now";
  } else if (diff < ONE_HOUR) {
    const minutes = Math.floor(diff / ONE_MINUTE);
    return `${minutes}m`;
  } else if (diff < ONE_DAY) {
    const hours = Math.floor(diff / ONE_HOUR);
    return `${hours}h`;
  } else {
    const days = Math.floor(diff / ONE_DAY);
    return `${days}d`;
  }
}

export function size(bytes: number) {
  const sizes = ["bytes", "KB", "MB"];
  if (bytes === 0) return "0 Bytes";
  const i = Math.floor(Math.log(bytes) / Math.log(1000));
  return `${parseFloat((bytes / Math.pow(1000, i)).toFixed(1))} ${sizes[i]}`;
}

export const isSuperagentError = (err: unknown): err is superagent.ResponseError =>
  typeof err === "object" &&
  err !== null &&
  "status" in err &&
  "response" in err &&
  typeof (err as superagent.ResponseError).response === "object" &&
  typeof (err.response as superagent.ResponseError["response"])?.status === "number";
