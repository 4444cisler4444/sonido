import { isSuperagentError } from ".";

const St8Empty = "St8::Empty" as const;
const St8Loading = "St8::Loading" as const;

export type St8<T> = typeof St8Empty | typeof St8Loading | St8Error | T;

/** Wait for all provided St8s to be ready. */
// @ts-expect-error Ignoring arg type def.
const _all: St8All = (...array) => array.find(arg => !St8.ready(arg)) || array;

interface St8All {
  <A>(ds1: St8<A>): St8<[A]>;
  <A, B>(ds1: St8<A>, ds2: St8<B>): St8<[A, B]>;
  <A, B, C>(ds1: St8<A>, ds2: St8<B>, ds3: St8<C>): St8<[A, B, C]>;
  <A, B, C, D>(ds1: St8<A>, ds2: St8<B>, ds3: St8<C>, ds4: St8<D>): St8<[A, B, C, D]>;
}

class St8Error extends Error {
  constructor(arg: unknown) {
    if (arg instanceof Error || isSuperagentError(arg)) {
      super(arg.message);

      Object.assign(this, arg);
    } else {
      super(String(arg));
    }
  }
}

// eslint-disable-next-line
export const St8 = {
  Empty: St8Empty,
  Loading: St8Loading,

  Error: (err: unknown): St8Error => {
    return new St8Error(err);
  },

  empty<T>(arg: St8<T>): arg is typeof St8Empty {
    return arg === St8Empty;
  },

  loading<T>(arg: St8<T>): arg is typeof St8Loading {
    return arg === St8Loading;
  },

  error<T>(arg: St8<T>): arg is St8Error {
    return arg instanceof St8Error;
  },

  ready<T>(arg: St8<T>): arg is T {
    return !St8.error(arg) && !St8.loading(arg) && arg !== St8Empty;
  },

  map<T, U>(arg: St8<T>, fn: (arg: T) => St8<U>): St8<U> {
    if (St8.ready(arg)) {
      return fn(arg);
    }
    return arg as St8<U>;
  },

  from<T>(state: { isLoading: boolean; error: unknown; data: T | undefined }): St8<T> {
    if (state.error) return St8.Error(state.error);
    if (state.isLoading) return St8.Loading;
    if (state.data === undefined) return St8.Empty;
    return state.data;
  },

  /**
   * Converts an array of St8 into a St8<T[]>.
   *
   * Providing generics is required for TypeScript to recognize the ready
   * result.
   *
   * @example St8.all<[string, number]>(str, num);
   */
  all: _all,
};
