<script setup lang="ts">
import { ref, computed } from 'vue';
import { AVWaveform } from 'vue-audio-visual';
import Response from 'superagent/lib/node/response';
import { useIsMutating } from '@tanstack/vue-query';

import { Comment, Nanosound } from '../interfaces';
import { format, formatRelative, notify, notifyPromise, St8, Paths, router } from '../util';
import {
  iconColor,
  isMobile,
  useCreateComment,
  useDeleteComment,
  useDeleteSound,
  userRef,
  useSounds,
  useUpdateSound,
  volume
} from '../api';
import St8View from './St8View.vue';

const { sound, isDetailView } = defineProps<{
  sound: Nanosound;
  isDetailView?: boolean;
  noWaveform?: boolean;
}>();

if (!sound || !sound.soundUrl) {
  notify('Sound not found, navigating home')
  router.push(Paths.home);
}

const src = decodeURIComponent(sound.soundUrl)

const isLoggedIn = computed(() => St8.ready(userRef.value));

const isPlaying = ref(false);
const showCommentInput = ref(false);
const commentText = ref("");
const commentedAt = ref(0); // seconds

const editing = ref<Nanosound | null>(null)
const newName = ref(sound.name)
const newDescription = ref(sound.description)

const updateSound = useUpdateSound();
const deleteSound = useDeleteSound()
const deleteComment = useDeleteComment()
const createComment = useCreateComment();

const isMutating = useIsMutating()

const isAuthor = computed(() =>
  St8.ready(userRef.value) ? sound.authorId === userRef.value.userId : false
);

const sounds = isDetailView ? useSounds(sound.authorId) : computed(() => St8.Empty);
const moreFromThisAuthor = computed(
  () => St8.map(sounds.value, ss => ss.filter(s => s.soundId !== sound.soundId).slice(0, 5))
);

const waveformWidth = computed(
  () => document.querySelector('#app')?.clientWidth ?? (isMobile ? 456 : 1068)
);

function onClick() {
  const audio: null | HTMLAudioElement = document.querySelector(`audio[src="${src}"]`);
  if (!audio) {
    console.warn('Audio not found');
    return;
  }

  const otherAudioPlaying = Array
    .from(document.querySelectorAll("audio"))
    .find(_ => !_.paused && _.src !== src)
  if (otherAudioPlaying) {
    otherAudioPlaying.pause();
  }

  audio.volume = volume.value / 100;

  if (!audio.ended) {
    audio.onended = () => {
      if (audio.currentTime === audio.duration) {
        audio.currentTime = 0;
      }
      isPlaying.value = false;
    }
  }

  if (!audio.onpause) {
    audio.onpause = () => {
      isPlaying.value = false;
    }
  }

  if (isPlaying.value) {
    audio.pause();
  } else {
    const p: Promise<void> = audio.play()
    if (p) {
      p
        .then(_ => {
          if (audio.paused) {
            return audio.play();
          }
        })
        .then(_ => {
          if (!audio.paused) {
            isPlaying.value = true;
            showCommentInput.value = true;
          }
        })
        .catch(err => {
          console.error(err)
        })
    }
  }
}

async function onDelete() {
  if (!window.confirm("Are you sure you want to delete this sound?")) {
    return;
  }

  try {
    const p = deleteSound({ soundId: sound.soundId });

    await notifyPromise(
      p,
      "Deleting...",
    )

    await router.push(Paths.home);

    notify(`Deleted ${sound.name}`);
  } catch (err) {
    if (err instanceof Error) {
      notify(err.message);
    } else if (err instanceof Response) {
      notify(err.error ? err.error.message : err.text)
    }

    console.error(err)
  }
}

async function onDeleteComment(c: Comment) {
  if (!window.confirm(`Delete comment by ${c.authorName}?`)) {
    return;
  }

  try {
    await notifyPromise(deleteComment(c), "Deleting...")
  } catch (err) {
    if (err instanceof Error) {
      notify(err.message);
    } else if (err instanceof Response) {
      notify(err.error ? err.error.message : err.text)
    }

    console.error(err)
  }
}

async function onUpdateSound() {
  if (!St8.ready(userRef.value)) {
    notify("Please log in to update your sound");
    return;
  }

  try {
    const promise = updateSound({
      ...sound,
      name: newName.value,
      description: newDescription.value,
    });

    await notifyPromise(promise, "Updating...")

    notify("Updated sound.");

    editing.value = null;
  } catch (err) {
    console.error(err);

    if (err instanceof Error) {
      notify(err.message);
    } else if (err instanceof Response) {
      notify(err.error ? err.error.message : err.text)
    }
  }
}
async function addComment() {
  if (!St8.ready(userRef.value)) {
    notify("Please log in to add a comment");
    return;
  }

  try {
    const promise = createComment({
      soundId: sound.soundId,
      text: commentText.value,
      commentedAt: commentedAt.value,
      authorName: userRef.value.username,
    });

    await notifyPromise(promise, "Adding comment...")

    showCommentInput.value = false;
  } catch (err) {
    console.error(err)

    if (err instanceof Error) {
      notify(err.message);
    } else if (err instanceof Response) {
      notify(err.error ? err.error.message : err.text)
    }
  } finally {
    commentText.value = "";
    commentedAt.value = 0;
  }
}

function setCommentedAt() {
  if (!St8.ready(userRef.value)) {
    console.warn('User not logged in');
    return;
  }

  const audio: null | HTMLAudioElement = document.querySelector(`audio[src="${src}"]`);
  if (!audio) {
    console.warn('Audio not found');
    return;
  }

  const seconds = Math.round(audio.currentTime);

  commentedAt.value = seconds;
}

const formatter = new Intl.DateTimeFormat('en-US', {
  minute: '2-digit',
  second: '2-digit',
  timeZone: 'UTC' // Use UTC to avoid timezone offsets
});

function formatSeconds(seconds: number) {
  const date = new Date(seconds * 1000); // Convert seconds to milliseconds
  return formatter.format(date).slice(1);
}
</script>

<template>
  <div class="space-y-8">
    <div class="flex justify-between items-start">
      <div class="flex space-x-2">
        <button @click="onClick" @click.capture="onClick"
          class="w-16 h-16 rounded-lg shadow-md relative font-medium text-4xl bg-none items-center justify-center">
          <span :class="{ 'rotate-90': isPlaying, 'pb-2': !isPlaying }"
            class="absolute inset-0 flex items-center justify-center">
            {{ isPlaying ? '&equals;' : '&vrtri;' }}
          </span>
        </button>
        <div class="flex flex-col">
          <p class="font-bold text-xl mb-1 bg-zinc-100 dark:bg-stone-900 p-1.5 leading-none w-fit hover:cursor-pointer" role="button"
             @click="router.push(Paths.sounds(sound.soundId))" data-testid="found-sound">
            {{ sound.name }}
          </p>
          <p class="text-base bg-zinc-100 dark:bg-stone-900 p-1.5 leading-none w-fit hover:cursor-pointer font-medium"
            @click="router.push(Paths.users(sound.authorId))">
            {{ sound.authorName }}
          </p>
        </div>
      </div>

      <div class="flex flex-col">
        <p class="text-gray-500 text-sm pl-2 font-medium">
          {{ format(sound.createdAt) }}
        </p>
        <button class="bg-transparent" v-if="isAuthor && isDetailView" @click="editing = editing ? null : sound">
          <v-icon name="io-settings"  :fill="iconColor" />
        </button>
      </div>
    </div>

    <div class="backdrop backdrop-opacity-80" v-if="isMutating > 0" />

    <div v-if="!!editing" class="flex flex-col space-y-4 p-6 max-w-screen-sm mx-auto">
      <div>
        <label for="newName" class="text-sm">New Name</label>
        <input type="text" id="newName" v-model="newName" class="p-2 w-full border border-gray-300 rounded" />
      </div>
      <div>
        <label for="newDescription" class="text-sm" :class="{ 'invisible': !newDescription }">
          New Description
        </label>
        <textarea rows="3" maxlength="200" id="newDescription" v-model="newDescription"
          placeholder="Add a description..." class="p-2 w-full border border-gray-300 rounded resize-none leading-5" />
      </div>

      <div class="flex space-x-6 items-end justify-end">
        <button @click="onDelete" class="bg-transparent shadow-sm">
          Delete sound
          <v-icon name="md-deleteforever-outlined" scale="1.2" hover :fill="'red'" />
        </button>
        <button @click="onUpdateSound" class="shadow-md">
          Update sound
          <v-icon name="co-save" scale="1.2" hover :fill="iconColor" />
        </button>
      </div>
    </div>


    <AVWaveform :src="src" :cors-anonym="true" :canv-height="isDetailView ? 225 : 150" :canv-width="waveformWidth"
      :audio-controls="false" :played-line-color="'rgb(255, 192, 233)'" :noplayed-line-color="'rgb(207, 202, 255)'"
      :playtime-with-ms="false" :playtime-font-size="16" :playtime-text-bottom="true"
      :playtime-font-family="'InterVariable,serif'" v-if="!noWaveform" />

    <div class="shadow-xl w-full space-y-4 p-6 rounded-md" v-if="isDetailView && !!sound.description.length">
      <p class="text-2xl font-bold">Sound Description</p>
      <p class="text-gray-500 font-medium" v-if="sound.description.length">
        {{ sound.description }}
      </p>
    </div>

    <div v-if="isDetailView || showCommentInput" :class="{ 'shadow-xl p-6 rounded-md space-y-4': isDetailView }">
      <p class="text-2xl font-bold" v-if="isDetailView">Comments</p>

      <div class="flex space-x-2 items-center" v-if="(isLoggedIn && showCommentInput) || isDetailView">
        <button class="rounded-full bg-zinc-50 shadow-sm dark:bg-stone-900 p-2 uppercase font-bold"
          v-if="!isMobile && St8.ready(userRef)" @click="router.push(Paths.users(userRef.userId))">
          {{ userRef.username[0] }}
        </button>

        <textarea rows="1" maxlength="80" placeholder="Write a comment"
          class="w-full resize-none rounded-full font-medium shadow-sm p-4 placeholder:opacity-80 leading-5 border border-slate-500/50"
          @click="showCommentInput = true, St8.ready(userRef) ? setCommentedAt : void 0" v-model="commentText" />

        <p v-if="commentedAt" class="font-bold">@{{ formatSeconds(commentedAt) }}</p>
      </div>

      <div class="flex justify-end w-full space-x-4" v-if="isDetailView && showCommentInput">
        <button @click="commentText = '', showCommentInput = false" class="rounded-full p-4 bg-transparent text-sm">
          Cancel
        </button>

        <button @click="addComment" class="whitespace-nowrap rounded-full p-4 pt-3 border border-gray-300/30 text-sm font-bold text-gray-600"
          :disabled="commentText.length < 2">
          Comment
          <v-icon name="bi-send" scale="1.2" hover :fill="iconColor" class="pt-1" />
        </button>
      </div>


      <div class="flex flex-col space-y-4">
        <div v-if="isDetailView" v-for="comment in sound.comments" class="flex space-x-2">
          <div class="flex justify-between w-full items-center font-medium">
            <span>
              <p class="font-bold">
                {{ comment.authorName }}
              </p>
              <p class="">
                {{ comment.text }}
              </p>
            </span>

            <div class="flex space-x-2 items-center text-gray-500">
              <p class="text-sm font-medium">
                {{ formatRelative(comment.createdAt) }}
              </p>
              <button class="bg-transparent pt-4 font-bold" @click="() => onDeleteComment(comment)">
                &TripleDot;
              </button>
            </div>
          </div>
        </div>

        <p class="text-sm text-gray-500 font-medium  hover:cursor-pointer" v-else-if="sound.comments.length > 0"
          @click="router.push(Paths.sounds(sound.soundId))">
          See {{ sound.comments.length }} comment{{ sound.comments.length > 1 ? 's' : '' }}...
        </p>
      </div>
    </div>

    <St8View :data="moreFromThisAuthor" v-if="isDetailView">
      <template #default="{ data }">
        <div class="shadow-xl p-6 rounded-md space-y-6">
          <p class="text-2xl font-bold">
            More from this artist
          </p>

          <SoundView v-for="sound in data" :key="sound.soundId" :sound="sound" noWaveform />
        </div>
      </template>
    </St8View>
  </div>
</template>

<style scoped>
.leading-custom {
  line-height: 0.5;
}
</style>
