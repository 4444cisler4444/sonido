import * as Sentry from "@sentry/vue";
import { QueryClientConfig, VueQueryPlugin } from "@tanstack/vue-query";
import { createApp } from "vue";
import { AVPlugin } from "vue-audio-visual";
import Vue3Toastify, { ToastContainerOptions } from "vue3-toastify";
import { OhVueIcon, addIcons } from "oh-vue-icons";
import {
  BiChevronRight,
  BiPerson,
  BiSend,
  IoSettings,
  CoSave,
  MdDeleteforeverOutlined,
  CoSearch,
} from "oh-vue-icons/icons";

import "vue3-toastify/dist/index.css";

import App from "./App.vue";
import { router } from "./util";

import "./style.css";

addIcons(BiPerson, BiSend, IoSettings, CoSave, MdDeleteforeverOutlined, CoSearch, BiChevronRight);

const MINUTE = 1000 * 60 * 60;

const vue3toastifyOptions: ToastContainerOptions = {
  position: "bottom-right",
  autoClose: 4000,
  disabledEnterTransition: true,
  // transition: 'none',
};

const queryClientConfig: QueryClientConfig = {
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      retry: 1,
      staleTime: MINUTE * 20,
      throwOnError: false,
    },
    mutations: {
      onError(err: Error) {
        if (import.meta.env.PROD) {
          return;
        }

        console.error(err);
      },
    },
  },
};

const app = createApp(App)
  .use(Vue3Toastify, vue3toastifyOptions)
  .use(AVPlugin)
  .use(router)
  .use(VueQueryPlugin, { queryClientConfig });

app.component("v-icon", OhVueIcon);

const dsn = import.meta.env.VITE_SENTRY_DSN;

Sentry.init({
  app,
  dsn,
  defaultIntegrations: false,
  integrations: [],
  autoSessionTracking: false,
  sendClientReports: false,
  beforeSend(event, _hint) {
    if (event.transaction === "window.focus") {
      return null;
    }
    return event;
  },
});

app.mount("#app");
