export interface Nanosound extends NanosoundBase {
  soundId: number;
  soundUrl: string;
  createdAt: number;

  /** Array of comments from comments table joined on `soundId`. */
  comments: Comment[];
}

export interface NanosoundBase {
  name: string;
  authorId: number;
  authorName: string;
  description: string;
}

export interface User {
  userId: number;
  username: string;
  email: string;
  disabled: boolean;
}

export interface CommentBase {
  text: string;
  soundId: number;
  authorName: string;
  commentedAt: number;
}

export interface Comment extends CommentBase {
  commentId: number;
  authorId: number;
  createdAt: number;
}
