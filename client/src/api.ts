import superagent from "superagent";
import { computed, Ref, ref } from "vue";
import { useRoute } from "vue-router";
import { toast } from "vue3-toastify";
import { useMutation, useQuery, useQueryClient } from "@tanstack/vue-query";
import { z } from "zod";

import { Nanosound, NanosoundBase, User, Comment, CommentBase } from "./interfaces";
import {
  isSuperagentError,
  notify,
  notifyIds,
  notifyPromise,
  Paths,
  router,
  snakeCase,
  St8,
} from "./util";
import { schemas } from "./schemas";

export const userRef: Ref<St8<User>> = ref(St8.Loading);

const retryCount = 2;

const LOCAL_URL = "http://localhost:8000";
const CLOUD_URL = "https://" + import.meta.env.VITE_EC2_CLOUD_URL;
const API_URL = (import.meta.env.DEV === false ? CLOUD_URL : LOCAL_URL) + "/api/v0";

export function deauthenticate() {
  localStorage.removeItem("sonido-jwt");
  localStorage.removeItem("sonido-refresh-token");

  userRef.value = St8.Empty;
}

export function authenticate(u: User, accessToken: string, refreshToken: string) {
  localStorage.setItem("sonido-jwt", accessToken);
  localStorage.setItem("sonido-refresh-token", refreshToken);
  sessionStorage.setItem("sonido-last-email", u.email);

  userRef.value = u;
}

export function token() {
  return localStorage.getItem("sonido-jwt");
}

const storedVol = localStorage.getItem("sonido-volume");
export const volume = ref<number>(storedVol ? +storedVol : 50);


async function reauth(err: unknown) {
  if (isSuperagentError(err) && err?.response?.status !== 401) {
    throw err;
  }

  await new Promise(done => setTimeout(done, 300));
  notifyIds.value.forEach(([message, id]) => {
    if (message.includes("Unauthorized")) {
      toast.remove(id);
    }
  });

  const refreshToken = localStorage.getItem("sonido-refresh-token");
  if (!refreshToken) {
    deauthenticate();
    notify("Session ended for security. Please log in and try again.");
    return;
  }

  return await superagent
    .post(API_URL + "/auth/refresh/")
    .send(refreshToken)
    .then(r => {
      if (!r.ok) throw r;

      const { accessToken, user, refreshToken } = schemas.AuthResp.parse(r.body);

      authenticate(user, accessToken, refreshToken);
    })
    .catch(err => {
      console.error("Failed to get refresh token for reauth: " + err);

      deauthenticate();
    });
}

export const queries = {
  users: {
    queryKey: ["users"],
    detail: (jwt: string) => ({
      queryKey: queries.users.queryKey.concat(jwt),
      async queryFn(): Promise<User> {
        return superagent
          .get(API_URL + "/auth/")
          .set("Authorization", `Bearer ${jwt}`)
          .then(r => {
            if (!r.ok) throw r;

            const u: User = schemas.User.parse(r.body);

            return (userRef.value = u);
          });
      },
    }),
  },
  sounds: {
    queryKey: ["sound"],
    detail: (soundId: string) => ({
      queryKey: queries.sounds.queryKey.concat(soundId),
      async queryFn(): Promise<St8<Nanosound>> {
        return superagent
          .get(API_URL + `/sounds/${soundId}`)
          .set("Accept", "application/json")
          .then(r => {
            if (!r.ok) throw r;

            return schemas.Nanosound.parse(r.body);
          })
          .catch(async err => {
            if (isSuperagentError(err) && err?.response?.status === 404) {
              await router.push(Paths.home);

              // notify("Failed to find that sound, navigating home.");
            }

            return St8.Error(err);
          });
      },
    }),
    list: (authorId?: number) => ({
      queryKey: authorId ? queries.sounds.queryKey.concat('' + authorId) : queries.sounds.queryKey,
      queryFn: (): Promise<Nanosound[]> =>
        superagent
          .get(API_URL + "/sounds/")
          .query({ authorId })
          .set("Accept", "application/json")
          .then(r => {
            if (!r.ok) throw r;

            return z.array(schemas.Nanosound).parse(r.body);
          }),
    }),
  },
};

export const isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;

const isDarkTheme = window.matchMedia?.("(prefers-color-scheme: dark)")?.matches;
export const iconColor = isDarkTheme ? "#f2f2f2" : "#000000";

export function useSounds(authorId?: number) {
  const q = useQuery(queries.sounds.list(authorId));

  return computed(() =>
    St8.from({
      error: q.error.value,
      isLoading: q.isLoading.value,
      data: q.data.value,
    }),
  );
}

export function useSound(soundId: string) {
  const queryClient = useQueryClient();

  const q = useQuery(queries.sounds.detail(soundId), queryClient);

  return computed(() =>
    St8.from({
      error: q.error.value,
      isLoading: q.isLoading.value,
      data: q.data.value,
    }),
  );
}

export async function trimFile(f: File) {
  const sixtyMegabytes = 60 * 1024 * 1024;
  if (f.size > sixtyMegabytes) {
    throw Error("File is too big. Max 60 MB");
  }

  const tkn = token();
  if (!tkn) {
    throw Error("Login required to trim a sound");
  }

  let format = f.type.split("/")[1].replace("x-", "");
  if (format === "mpeg") {
    format = "mp3";
  }

  return superagent
    .post(API_URL + "/sounds/trim")
    .attach("file", f as any)
    .query({
      format,
      filename: f.name,
    })
    .set("Authorization", `Bearer ${tkn}`)
    // .set("Accept", "application/json")
    .then(r => {
      if (!r.ok) throw r;
      // if (!(r.body instanceof Blob)) {
      //   throw TypeError("Expected blob, got " + r.body);
      // }
      // return r.body.arrayBuffer();
    })
  // .then(audioBytes => {
  //   return new File([audioBytes], f.name, f);
  // });
}

export function useLogIn() {
  const queryClient = useQueryClient();
  return useMutation({
    async mutationFn(email: string): Promise<User> {
      return superagent
        .post(API_URL + "/auth/token/")
        .query({ email })
        .then(r => {
          if (!r.ok) throw r;

          const { accessToken, user, refreshToken } = schemas.AuthResp.parse(r.body);

          authenticate(user, accessToken, refreshToken);

          return user;
        });
    },
    onSuccess: user => queryClient.setQueryData(queries.users.queryKey, user),
  }).mutateAsync;
}

export function useSignUp() {
  const queryClient = useQueryClient();
  return useMutation({
    async mutationFn(email: string): Promise<User> {
      return superagent
        .post(API_URL + "/auth/")
        .query({ email })
        .then(r => {
          if (!r.ok) throw r;

          const { accessToken, user, refreshToken } = schemas.AuthResp.parse(r.body);

          authenticate(user, accessToken, refreshToken);

          return user;
        });
    },
    onSuccess: user => queryClient.setQueryData(queries.users.queryKey, user),
  }).mutateAsync;
}

export function useUpdateUsername() {
  const queryClient = useQueryClient();
  const m = useMutation({
    async mutationFn(newUsername: string): Promise<User> {
      const jwt = token();
      if (!jwt) {
        throw Error("Login required to update username");
      }

      return superagent
        .patch(API_URL + "/auth/")
        .send({ newUsername })
        .set("Authorization", `Bearer ${jwt}`)
        .retry(retryCount, async err => {
          await reauth(err);
        })
        .then(r => {
          if (!r.ok) throw r;

          return schemas.User.parse(r.body);
        });
    },
    onSuccess: (u: User) => queryClient.setQueryData(queries.users.queryKey, u),
  });

  return m.mutateAsync;
}

export const searched = async (query: string) =>
  superagent
    .get(API_URL + "/search/")
    .query({ query })
    .then(r => {
      if (!r.ok) throw r;

      return schemas.Search.parse(r.body);
    });

export function useUploadSound() {
  const queryClient = useQueryClient();
  const route = useRoute();

  const m = useMutation({
    async mutationFn({ file, ...soundbase }: NanosoundBase & { file: File }): Promise<Nanosound> {
      const fdata = new FormData();
      fdata.append("ufile", file);

      const tkn = token();
      if (!tkn) {
        throw Error("Login required to share sounds");
      }

      return superagent
        .post(API_URL + "/sounds/")
        .set("Authorization", `Bearer ${tkn}`)
        .set("Accept", "application/json")
        .query({ jsonstr: JSON.stringify(snakeCase(soundbase)) })
        .send(fdata)
        .retry(retryCount, async err => {
          await reauth(err);
        })
        .then(r => {
          if (!r.ok) throw r;
          return r.body;
        })
    },
    onSuccess: () => {
      const soundId = route.params.soundId;
      if (soundId && typeof soundId === "string") {
        return queryClient.invalidateQueries(queries.sounds.detail(soundId));
      }

      return queryClient.invalidateQueries({ queryKey: queries.sounds.queryKey });
    },
  });

  return m.mutateAsync;
}

export function useUpdateSound() {
  const queryClient = useQueryClient();

  const { mutateAsync } = useMutation({
    async mutationFn(fields: Nanosound): Promise<Nanosound> {
      const tkn = token();
      if (!tkn) {
        throw Error("Login required to update sounds");
      }

      return superagent
        .put(API_URL + `/sounds/${fields.soundId}`)
        .set("Authorization", `Bearer ${tkn}`)
        .send(fields)
        .retry(retryCount, async err => {
          await reauth(err);
        })
        .then(async r => {
          if (!r.ok) throw r;
          // return validated fields from the server
          return fields;
        });
    },
    onSuccess: (fields: Nanosound) =>
      queryClient.invalidateQueries(queries.sounds.detail(String(fields.soundId))),
  });

  return mutateAsync;
}

export function useDeleteSound() {
  const queryClient = useQueryClient();

  const { mutateAsync } = useMutation({
    async mutationFn({ soundId }: Pick<Nanosound, "soundId">): Promise<number> {
      const tkn = token();
      if (!tkn) {
        throw Error("Login required to delete sounds");
      }

      return superagent
        .delete(API_URL + `/sounds/${soundId}`)
        .set("Authorization", `Bearer ${tkn}`)
        .retry(retryCount, async err => {
          await reauth(err);
        })
        .then(r => {
          if (!r.ok) throw r;
          // delete endpoints don't return anything, only a 2XX status code
          return soundId;
        });
    },
    onSuccess: (soundId) =>
      queryClient.setQueryData<Nanosound[]>(queries.sounds.queryKey, (sounds = []) =>
        sounds.filter(_ => _.soundId !== soundId)
      )
  });

  return mutateAsync;
}

export function useCreateComment() {
  const queryClient = useQueryClient();
  const m = useMutation({
    async mutationFn(fields: CommentBase) {
      const tkn = token();
      if (!tkn) {
        throw Error("Login required to add comments");
      }

      return superagent
        .post(API_URL + '/comments/')
        .set("Authorization", `Bearer ${tkn}`)
        .set("Accept", "application/json")
        .send(fields)
        .retry(retryCount, async err => {
          await reauth(err);

          if (m.failureReason.value) {
            m.mutateAsync(fields);
          }
        })
        .then(r => {
          if (!r.ok) throw r;
          return schemas.Comment.parse(r.body);
        });
    },
    onSuccess: (parsed: Comment) =>
      queryClient.invalidateQueries(queries.sounds.detail(String(parsed.soundId))),
  });

  return m.mutateAsync;
}

export function useDeleteComment() {
  const queryClient = useQueryClient();
  const m = useMutation({
    async mutationFn(comment: Comment) {
      const tkn = token();
      if (!tkn) {
        throw Error("Login required to delete comments");
      }

      return superagent
        .delete(API_URL + `/comments/${comment.commentId}/`)
        .set("Authorization", `Bearer ${tkn}`)
        .retry(retryCount, async err => {
          await reauth(err);

          // Nonvalid retries have a null or undefined failureReason
          if (!!m.failureReason.value) {
            m.mutateAsync(comment);
          }
        })
        .then(r => {
          if (!r.ok) throw r;

          // return server-validated data
          return comment;
        });
    },
    onSuccess: (deletedComment: Comment) =>
      queryClient.invalidateQueries(queries.sounds.detail(String(deletedComment.soundId))),
  });

  return m.mutateAsync;
}
