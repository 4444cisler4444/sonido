import { z } from "zod";

const UserSchema = z.object({
  userId: z.number(),
  username: z.string(),
  email: z.string(),
  disabled: z.boolean(),
});

const TokenSchema = z.object({
  accessToken: z.string(),
  tokenType: z.string(),
});

const CommentSchema = z.object({
  commentId: z.number(),
  authorId: z.number(),
  createdAt: z.number(),
  text: z.string(),
  soundId: z.number(),
  commentedAt: z.number(),
  authorName: z.string(),
});

const NanosoundSchema = z.object({
  soundId: z.number(),
  soundUrl: z.string(),
  createdAt: z.number(),
  comments: z.array(CommentSchema),
  name: z.string(),
  authorId: z.number(),
  authorName: z.string(),
  description: z.string(),
})

// See main.py classes
export const schemas = {
  AuthResp: z.object({
    ...TokenSchema.shape,
    user: UserSchema,
    refreshToken: z.string(),
  }),
  User: UserSchema,
  Comment: CommentSchema,
  Nanosound: NanosoundSchema,
  Search: z.object({
    sounds: z.array(NanosoundSchema),
    users: z.array(UserSchema),
  })
};
