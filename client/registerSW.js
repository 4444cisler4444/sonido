console.log('service worker file...');

if ('serviceWorker' in navigator) {
  window.addEventListener('load', async () => {
    try {
      const _r =
        await navigator.serviceWorker.register('/service-worker.js', { scope: '/' });

      console.log('SW registered: ', _r);
    } catch (err) {
      console.error(err);

      console.log('Failed to register service worker');
    }
  });
}
