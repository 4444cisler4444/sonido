from datetime import datetime, timedelta, timezone
from typing import Annotated
from uuid import uuid4
from os import environ, uname
import logging

from fastapi import Body, Depends, status, APIRouter
import jwt
from jwt.exceptions import InvalidTokenError
from psycopg2.extras import RealDictCursor
from pydantic import validate_email
from starlette.exceptions import HTTPException

from app.dependencies import db_dependency, SECRET_KEY, get_user, current_user
from app.internal.view_models import AuthResp, UserModel, TokenData
from app.internal import models


router = APIRouter(
    prefix="/auth",
    tags=["auth"],
)

prod = 'aws' in uname().release
ISSUER = environ.get(
    "ISSUER",
    environ["VITE_EC2_CLOUD_URL"] if prod else "http://localhost:8000"
)
AUDIENCE = environ.get(
    "AUDIENCE",
    "https://sonido.vercel.app" if prod else "http://localhost:5173"
)

ACCESS_TOKEN_EXPIRE_MINUTES = 90

# JWT_SECRET_KEY = "supersecretkey"


@router.post("/refresh", response_model=AuthResp)
async def refresh_token(db: db_dependency, refresh_token: str = Body(...)):
    creds_err = HTTPException(
        status_code=status.HTTP_400_BAD_REQUEST,
        detail="Credentials invalid for token refresh",
        headers={"WWW-Authenticate": "Bearer"},
    )

    try:
        logging.info(f"Decoding given refresh token: {refresh_token}")
        decoded = jwt.decode(
            refresh_token,
            SECRET_KEY,
            algorithm="HS256",
            options={"verify_signature": False},
            issuer=ISSUER,
            audience=AUDIENCE,
        )

        username = decoded.get("sub")
        if username is None:
            creds_err.detail += "No username in token"
            raise creds_err

        token_data = TokenData(username=username)

    except InvalidTokenError as e:
        if e.args[0] == "exp":
            creds_err.detail += "Token has expired"
            raise creds_err

        creds_err.detail += "Invalid token"
        raise creds_err

    logging.info(f"Getting user for refresh token data: {token_data}")
    u = get_user(token_data.username, db)
    if u is None:
        creds_err.detail += "User not found"
        raise creds_err

    new_access, new_refresh = define_token(
        {"sub": token_data.username, "id": u.user_id}
    )
    user = UserModel(**u.__dict__)

    return AuthResp(
        user=user,
        access_token=new_access,
        token_type="bearer",
        refresh_token=new_refresh,
    )


@router.get("/", status_code=status.HTTP_200_OK, response_model=UserModel)
async def user(u: Annotated[models.UserInDb, Depends(current_user)]):
    return UserModel(**u.__dict__)


@router.post("/token", response_model=AuthResp)
async def login_for_jwt(db: db_dependency, email: str):
    logging.info(
        f"Getting user by email for login for jwt given an email: {email}")
    u = get_user_by_email(email, db)
    if not u:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid email",
            headers={"WWW-Authenticate": "Bearer"},
        )

    tkn, refresh_token = define_token(
        data={"sub": u.username, "id": u.user_id})
    user = UserModel(**u.__dict__)

    return AuthResp(
        user=user, access_token=tkn, token_type="bearer", refresh_token=refresh_token
    )


@router.post("/", response_model=AuthResp, status_code=status.HTTP_201_CREATED)
async def create_user(db: db_dependency, email: str):
    logging.info(f"Validating email for user record creation: {email}")
    username, email = validate_email(email)

    with db.cursor(cursor_factory=RealDictCursor) as cur:
        logging.info(
            "Checking if user already exists before creating new user")
        cur.execute("SELECT * FROM sonido_users WHERE email = %s", (email,))
        if cur.fetchone() is not None:
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail="User with that email already exists",
            )
        logging.info(
            "Duplicate email not detected, this is good, creating user...")

        cur.execute(
            "INSERT INTO sonido_users (username, email, disabled) VALUES (%s, %s, %s) RETURNING *",
            (username, email, False),
        )
        db.commit()
        logging.info("User created successfully")

        rowdict = cur.fetchone()
        if rowdict is None or "user_id" not in rowdict:
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail="Could not create user",
            )

        u = UserModel(**rowdict)

        logging.info(
            f"Creating jwt and refresh token for newly created user: {u}")
        tkn, refresh_token = define_token(
            data={"sub": u.username, "id": u.user_id})

        return AuthResp(
            user=u, access_token=tkn, token_type="bearer", refresh_token=refresh_token
        )


def get_user_by_email(email: str, db: db_dependency) -> models.UserInDb | None:
    logging.info(f"Getting user by email: {email}")
    with db.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute("SELECT * FROM sonido_users WHERE email = %s", (email,))

        rowdict = cur.fetchone()
        if rowdict is None or "user_id" not in rowdict:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Could not find user",
                headers={"WWW-Authenticate": "Bearer"},
            )

        logging.info(f"Successfully retrieved user by email: {rowdict}")
        return models.UserInDb(**rowdict)


def define_token(data: dict) -> tuple[str, str]:
    # access_token_expiry = datetime.now(timezone.utc) + timedelta(seconds=2)
    access_token_expiry = datetime.now(
        timezone.utc) + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    refresh_token_expiry = datetime.now(timezone.utc) + timedelta(hours=21)

    copy = data.copy()
    copy.update({
        "iat": datetime.now(timezone.utc),
        "iss": ISSUER,
        "aud": AUDIENCE,
    })

    access_tkn = jwt.encode(
        {**copy, "exp": access_token_expiry},
        SECRET_KEY,
        algorithm="HS256"
    )
    refresh_tkn = jwt.encode(
        {**copy, "exp": refresh_token_expiry, "jti": str(uuid4())},
        SECRET_KEY,
        algorithm="HS256",
    )

    logging.info("Generated jwt and refresh token")
    return access_tkn, refresh_tkn
