from urllib import parse
from datetime import datetime
from typing import Annotated, List
import io
import os
import tempfile
import logging
import asyncio

from fastapi.responses import StreamingResponse
from pydub import AudioSegment
from fastapi import APIRouter, Depends, status, UploadFile, Body
from psycopg2.extras import RealDictCursor
from starlette.exceptions import HTTPException

from app.dependencies import db_dependency, current_user, storage
from app.internal import models
from app.internal.view_models import (
    NanosoundModel,
    NanosoundBase,
    UserModel,
    CommentModel,
)


router = APIRouter(
    prefix="/sounds",
    tags=["sounds"],
)


# Because of how jsonstr needs to be passed in, it's better to test this using
# the client rather than swagger/openapi
@router.post("/", response_model=NanosoundModel)
async def create_nanosound(
    jsonstr: str,
    ufile: UploadFile,
    db: db_dependency,
    u: Annotated[models.UserInDb, Depends(current_user)],
):
    fields = NanosoundBase.model_validate_json(jsonstr)

    logging.info(f"Checking for existing sound: {fields}")
    with db.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute(
            "SELECT * FROM nanosounds WHERE name = %s AND author_id = %s",
            (fields.name, fields.author_id),
        )
        sound = cur.fetchone()
        logging.info(f"Found existing sound: {sound}")
        if sound:
            logging.info(f"Updating sound: {fields}")
            return upsert_sound(sound["sound_url"], fields, db)

    fpath = f"{u.username}/{parse.quote(fields.name)}"
    f = await ufile.read()

    logging.info(f"Uploading file to storage: {fpath}")
    _ = storage.upload(
        fpath,
        f,
        file_options={"content-type": "audio/*", "upsert": "true"}
    )
    logging.info(f"Successfully uploaded file to storage: {fpath}")

    sound_url = storage.get_public_url(fpath)

    return upsert_sound(sound_url, fields, db)


def upsert_sound(sound_url: str, fields: NanosoundBase, db: db_dependency):
    created_at = datetime.now().timestamp() * 1000

    logging.info(f"Upserting sound into database: {fields}")
    with db.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute(
            """
            INSERT INTO nanosounds (sound_url, created_at, author_name, author_id, name, description)
            VALUES (%s, %s, %s, %s, %s, %s)
            ON CONFLICT (sound_id) DO UPDATE
            SET sound_url = EXCLUDED.sound_url,
                created_at = EXCLUDED.created_at,
                author_name = EXCLUDED.author_name,
                author_id = EXCLUDED.author_id,
                name = EXCLUDED.name,
                description = EXCLUDED.description
            RETURNING sound_id;
            """,
            (
                sound_url,
                created_at,
                fields.author_name,
                fields.author_id,
                fields.name,
                fields.description,
            ),
        )
        db.commit()

        rowdict = cur.fetchone()
        if rowdict is None or "sound_id" not in rowdict:
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail="Error creating sound",
            )

        sound_id = rowdict["sound_id"]

        return NanosoundModel(
            sound_id=sound_id,
            sound_url=sound_url,
            created_at=created_at,
            name=fields.name,
            author_name=fields.author_name,
            author_id=fields.author_id,
            description=fields.description,
            comments=[],
        )


@router.put("/{sound_id}", status_code=status.HTTP_204_NO_CONTENT)
async def update_nanosound(
    sound_id: int,
    db: db_dependency,
    user: Annotated[UserModel, Depends(current_user)],
    sound: NanosoundBase = Body(...),
):
    logging.info(f"Updating sound: {sound}")
    with db.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute(
            "UPDATE nanosounds SET name = %s, description = %s WHERE sound_id = %s AND author_id = %s RETURNING *",
            (sound.name, sound.description, sound_id, user.user_id),
        )
        db.commit()
        logging.info(f"Updated sound: {sound}")


@router.get("/{sound_id}", response_model=NanosoundModel)
async def read_nanosound(sound_id: int, db: db_dependency):
    logging.info(f"Retreiving sound: {sound_id}")
    with db.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute("SELECT * FROM nanosounds WHERE sound_id = %s", (sound_id,))
        logging.info(f"Retreived sound: {sound_id}")

        rowdict = cur.fetchone()
        if rowdict is None or "sound_id" not in rowdict:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Could not find sound",
            )

        logging.info(f"Retreiving comments for sound: {sound_id}")
        cur.execute(
            "SELECT * FROM sonido_comments WHERE sound_id = %s", (sound_id,))
        rows = cur.fetchmany(size=100)
        logging.info(f"Retreived {len(rows)} comments for sound: {sound_id}")

        comments = [CommentModel(**r) for r in rows]
        comments = sorted(comments, key=lambda c: c.created_at, reverse=True)

        return NanosoundModel(**rowdict, comments=comments)


@router.get("/", response_model=List[NanosoundModel])
def read_all_nanosounds(
    db: db_dependency,
    limit: int = 50,
    authorId: int | None = None
):
    author_id = authorId
    logging.info(f"Retreiving all sounds for user: {author_id}")
    with db.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute(
            f"""
            SELECT sound.*,
              (
                SELECT
                JSON_AGG(
                    JSON_BUILD_OBJECT(
                        'text', c.text,
                        'soundId', c.sound_id,
                        'authorId', c.author_id,
                        'authorName', c.author_name,
                        'commentId', c.comment_id,
                        'commentedAt', c.commented_at,
                        'createdAt', c.created_at
                    )
                )
                FROM sonido_comments c
                WHERE sound.sound_id = c.sound_id
              ) AS comments
            FROM nanosounds sound
            WHERE (author_id = %s or %s is NULL)
            ORDER BY sound.created_at DESC
            LIMIT %s;
            """,
            (
                author_id,
                author_id,
                limit,
            ),
        )
        rows = cur.fetchmany(size=limit)
        logging.info(f"Retreived {len(rows)} sounds")

        for r in rows:
            comments = r.get("comments") or []
            if len(comments):
                comments = sorted(comments, key=lambda c: c["createdAt"])
            r.update({"comments": comments})

        return rows


@router.delete("/{sound_id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_nanosound(
    sound_id: int,
    db: db_dependency,
    u: Annotated[models.UserInDb, Depends(current_user)],
):
    user = UserModel(**u.__dict__)

    logging.info(f"Deleting sound: {sound_id}")
    with db.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute(
            "DELETE FROM nanosounds WHERE sound_id = %s AND author_id = %s",
            (sound_id, user.user_id),
        )
        db.commit()
        logging.info(f"Deleted sound: {sound_id}")

        return {}


@router.post("/trim", response_class=StreamingResponse)
async def trim_audio_file(
    file: UploadFile,
    format: str,
    filename: str,
    u: Annotated[models.UserInDb, Depends(current_user)],
    db: db_dependency
):
    logging.info(f"Creating temp file for format '{format}'")
    tmp = tempfile.NamedTemporaryFile(delete=False, suffix=f".{format}")
    logging.info(f"Temporary file created successfully at path: {tmp.name}")

    logging.info(f"Reading audio file bytes: {file.filename}")
    buf = await file.read()
    fbytes = io.BytesIO(buf)
    logging.info("Audio file bytes read successfully")

    logging.info(f"Trimming audio file: {file.filename}")
    trimmed_audio = AudioSegment.from_file_using_temporary_files(
        # linter on my case about `fbytes` `BytesIO` type and `bytes` base type
        fbytes, format=format, duration=21  # pyright: ignore
    )
    logging.info(f"Audio file trimmed successfully: {file.filename}")

    logging.info(f"Exporting trimmed audio file: {file.filename}")
    fmt = "mp3" if format == "mpeg" else format
    trimmed_audio.export(tmp.name, format=fmt)
    logging.info(f"Trimmed audio file exported successfully: {file.filename}")

    buf_trimmed = tmp.read()

    tmp.close()  # will this close delete the file on prod OS?
    logging.info("Closed temporary file handle")

    # Promise.resolve().then(() => os.unlink(tmp.name));
    asyncio.create_task(asyncio.to_thread(os.unlink, tmp.name))

    fpath = f"{u.username}/{parse.quote(filename)}"
    soundbase = NanosoundBase(
        name=filename,
        author_id=u.user_id,
        author_name=u.username,
        description="",
    )

    logging.info(f"Uploading file to storage: {fpath}")
    _ = storage.upload(
        fpath,
        buf_trimmed,
        file_options={"content-type": "audio/*", "upsert": "true"}
    )
    logging.info("Successfully uploaded trimmed file to storage: {fpath}")

    sound_url = storage.get_public_url(fpath)

    return upsert_sound(sound_url, soundbase, db)
