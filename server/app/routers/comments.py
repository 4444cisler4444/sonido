from typing import Annotated
from datetime import datetime
import logging

from fastapi import APIRouter, Depends, status
from psycopg2.extras import RealDictCursor
from starlette.exceptions import HTTPException

from app.internal import models
from app.dependencies import db_dependency, current_user
from app.internal.view_models import UserModel, CommentModel, CommentBase


router = APIRouter(
    prefix="/comments",
    tags=["comments"],
)


@router.post("/", response_model=CommentModel, status_code=status.HTTP_201_CREATED)
def create_comment(
    comment: CommentBase,
    db: db_dependency,
    u: Annotated[models.UserInDb, Depends(current_user)]
):
    logging.info(f"Creating comment: {comment}")
    with db.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute(
            "SELECT * FROM nanosounds WHERE sound_id = %s",
            (comment.sound_id,)
        )

        rowdict = cur.fetchone()
        if rowdict is None or "sound_id" not in rowdict or rowdict.get('sound_id') != comment.sound_id:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Sound not found with the given id",
            )

        logging.info(f"Successfully retrieved sound for comment: {comment.sound_id}")

        # timestamp in milliseconds
        created_at = datetime.now().timestamp() * 1000

        logging.info(f"Creating comment: {comment}")
        cur.execute(
            "INSERT INTO sonido_comments (sound_id, author_id, author_name, text, commented_at, created_at) VALUES (%s, %s, %s, %s, %s, %s) RETURNING *",
            (
                comment.sound_id,
                u.user_id,
                u.username,
                comment.text,
                comment.commented_at,
                created_at
            )
        )
        db.commit()

        rowdict = cur.fetchone()
        if rowdict is None or "comment_id" not in rowdict:
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail="Error creating comment",
            )

        c = CommentModel(**rowdict)
        logging.info(f"Successfully created comment: {c}")
        return c


@router.delete("/{comment_id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_comment(
    comment_id: int,
    db: db_dependency,
    u: Annotated[models.UserInDb, Depends(current_user)]
):
    logging.info(f"Deleting comment: {comment_id}")
    user = UserModel(**u.__dict__)

    with db.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute(
            "DELETE FROM sonido_comments WHERE comment_id = %s AND author_id = %s",
            (comment_id, user.user_id),
        )
        db.commit()

        logging.info(f"Successfully deleted comment: {comment_id}")
