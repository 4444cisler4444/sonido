from datetime import datetime, timezone
from os import environ
from typing import Annotated
import jwt

from fastapi import Depends, status
from fastapi.security import OAuth2PasswordBearer
from jwt.exceptions import InvalidTokenError
from psycopg2.extensions import connection
from psycopg2.extras import RealDictCursor
from starlette.exceptions import HTTPException
from supabase import create_client
import psycopg2

from app.internal import models
from app.internal.view_models import TokenData


surl = environ["SUPABASE_URL"]
skey = environ["SUPABASE_KEY"]
storage = create_client(surl, skey).storage.get_bucket("nanosounds")


def get_db():
    db = psycopg2.connect(environ["DB_CONN_STRING"])

    try:
        yield db
    except Exception:
        db.close()
        raise


db_dependency = Annotated[connection, Depends(get_db)]
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

SECRET_KEY = "80a9fb8b4d31741703322d0d748adb814f156a10ff79fed7bc262e18ba20b8c2"


def decode(token: str):
    return jwt.decode(
        token,
        SECRET_KEY,
        algorithm="HS256",
        options={'verify_signature': False}
    )


def current_user(
    token: Annotated[str, Depends(oauth2_scheme)],
    db: db_dependency
) -> models.UserInDb:
    creds_err = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Credentials invalid",
        headers={"WWW-Authenticate": "Bearer"}
    )

    try:
        decoded = jwt.decode(
            token,
            SECRET_KEY,
            algorithm="HS256",
            options={'verify_signature': False}
        )

        username = decoded.get("sub")
        if username is None:
            creds_err.detail = "Credentials invalid: No username in token"
            raise creds_err

        expiry = decoded.get("exp")
        if expiry is None:
            creds_err.detail = "Credentials invalid: No expiry in token"
            raise creds_err
        if expiry < datetime.now(timezone.utc).timestamp():
            creds_err.detail = "Credentials invalid: Token expired"
            raise creds_err

        token_data = TokenData(username=username)
    except InvalidTokenError:
        creds_err.detail = "Credentials invalid: Invalid token"
        raise creds_err

    u = get_user(token_data.username, db)
    if u is None:
        creds_err.detail = "Credentials invalid: User not found"
        raise creds_err

    return u


def get_user(username: str, db: db_dependency) -> models.UserInDb | None:
    with db.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute(
            "SELECT * FROM sonido_users WHERE username = %s",
            (username,)
        )

        rowdict = cur.fetchone()
        if rowdict is None or "user_id" not in rowdict:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Could not find user",
                headers={"WWW-Authenticate": "Bearer"},
            )

        return models.UserInDb(**rowdict)
