from dotenv import load_dotenv
import logging
import sys

from psycopg2.extras import RealDictCursor
from starlette.exceptions import HTTPException
from starlette.middleware.cors import CORSMiddleware
from fastapi import FastAPI, Request

from app.internal.view_models import NanosoundModel, UserModel
from app.routers import comments, sounds, auth
from app.dependencies import db_dependency

logging.basicConfig(
    level=logging.DEBUG,
    stream=sys.stdout,
    format="%(asctime)s - %(levelname)s - %(message)s"
)

load_dotenv()

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_methods=["*"],
    allow_headers=["*"],
    allow_credentials=True,
    allow_origins=["http://localhost:5173", "https://sonido.vercel.app"]
)

prefix = "/api/v0"

app.include_router(sounds.router, prefix=prefix)
app.include_router(comments.router, prefix=prefix)
app.include_router(auth.router, prefix=prefix)


@app.get(prefix + "/search", tags=["search"])
async def search(query: str, db: db_dependency):
    if len(query.strip()) < 3:
        raise HTTPException(400, "Query must be at least 3 characters long")

    sounds = []
    users = []
    with db.cursor(cursor_factory=RealDictCursor) as cur:
        logging.info(f"Searching for query: {query}")
        cur.execute("SELECT * FROM nanosounds WHERE name ILIKE %s", (f"%{query}%",))

        rowdicts = cur.fetchmany(size=13)
        for rowdict in rowdicts:
            sounds.append(NanosoundModel(**rowdict, comments=[]))
        logging.info(f"Found {len(sounds)} sounds for query: {query}")

        cur.execute("SELECT * FROM sonido_users WHERE username ILIKE %s", (f"%{query}%",))

        rowdicts = cur.fetchmany(size=13)
        for rowdict in rowdicts:
            users.append(UserModel(**rowdict))
        logging.info(f"Found {len(users)} users/artists for query: {query}")

    return {"sounds": sounds, "users": users}


@app.exception_handler(Exception)
async def global_exception_handler(_: Request, exc: Exception):
    logging.error(f"Unhandled exception: {exc}")

    raise HTTPException(500, "Internal Server Error")
