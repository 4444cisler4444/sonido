from fastapi.testclient import TestClient

from .main import app

client = TestClient(app)


def test_read_all_sounds():
    response = client.get("/api/v0/sounds")
    assert response.status_code == 200

    sounds = response.json()
    assert len(sounds) > 0

    assert "comments" in sounds[0]
    assert len(sounds[0]["comments"]) >= 0
