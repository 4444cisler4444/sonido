from sqlalchemy import Column, ForeignKey, String, Integer, Boolean, Float
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.pool import NullPool

from dotenv import load_dotenv
from os import environ

load_dotenv()

connstr = environ["DB_CONN_STRING"]

engine = create_engine(connstr, poolclass=NullPool)

Base = declarative_base()


class NanosoundInDb(Base):
    __tablename__ = "nanosounds"

    sound_id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    author_id = Column(Integer)
    author_name = Column(String)
    sound_url = Column(String)
    created_at = Column(Float)
    description = Column(String)


class UserInDb(Base):
    __tablename__ = "sonido_users"

    user_id = Column(Integer, primary_key=True, index=True)
    username = Column(String)
    email = Column(String)
    disabled = Column(Boolean)


class CommentInDb(Base):
    __tablename__ = "sonido_comments"

    comment_id = Column(Integer, primary_key=True, index=True)
    sound_id = Column(
        Integer, ForeignKey("nanosounds.sound_id", ondelete="CASCADE"), nullable=False
    )
    author_id = Column(
        Integer, ForeignKey("sonido_users.user_id", ondelete="CASCADE"), nullable=False
    )
    author_name = Column(String, nullable=False)
    text = Column(String, nullable=False)
    created_at = Column(Float, nullable=False)
    commented_at = Column(Integer, nullable=False)


Base.metadata.create_all(bind=engine)
