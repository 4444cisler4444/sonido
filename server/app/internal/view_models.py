from typing import List

from fastapi_camelcase import CamelModel


class Token(CamelModel):
    access_token: str
    token_type: str


class TokenData(CamelModel):
    username: str


class UserModel(CamelModel):
    username: str
    email: str
    user_id: int
    disabled: bool


class AuthResp(Token):
    user: UserModel
    refresh_token: str


class CommentBase(CamelModel):
    text: str
    sound_id: int
    author_name: str
    commented_at: int


class CommentModel(CommentBase):
    comment_id: int
    created_at: float
    author_id: int


class NanosoundBase(CamelModel):
    name: str
    author_id: int
    author_name: str
    description: str


class NanosoundModel(NanosoundBase):
    sound_id: int
    sound_url: str
    created_at: float
    comments: List[CommentModel]
