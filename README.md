## [sonido](https://sonido.vercel.app)
Sonido is motivated by a desire to upload and share music ideas and own the platform that makes it
possible. This is also built for gaining expertise in [FastAPI](https://fastapi.tiangolo.com/),
[Ansible](https://www.ansible.com/), [AWS](https://aws.amazon.com), and [VueJS](https://vuejs.org) .

Join, listen, and share :)

![Architecture Diagram](https://mermaid.ink/img/pako:eNp9lV1v2jAUhv-KFal37Sr1kotJaRKglI8UAu1m0OTFLlhL7MxOVFVV__uMTwwOhXERkjyv3-Njnxx_BLmkLOgFW0WqHRrP1wKZX4j7RNdh-oDCqtqgm5vv6B5POKUFeyOK9VA0my-Oz5t2lBVGOCY1-U00Q5EUguU1l6KjiPHoOUNhU--YqHlOvggSvNRMoUTQSnJR6w7s4ykRUstG0AuKAY5kWRrvC3yIw4Zyifq8YChVMmdac7FtNXCNrPIBV_o9l9X27lsOuWx8PDqm-tQwxZnueMRW9Ihn-1TvUqL1m1T0npkVUxtfMrYLksk_TKAhEbQ4zgYEE-xGG653p5NNrGiKB0mGbokJtvHfz3A6W7Tgtt4H6eDUwx3XvsVPLbYL7vIDNMfp8kDg7xenHckC5vQ_SeZLOmSJ42ScZMml8XAdWO2qnWYOO--MAD4fjBx2N6dmQ6t_ac1qxcsOvrpCMXvlgiHqNv4vbDzwkR3-A8q3WxKAfnrFe46H4aF2z1XUMf6-YOxeol1bMKAYg809bnWZt98ti3DUKLUPYae5IgWn_jd4Gqr7LQKdglUMmU5MDynQnOlKCu26wQwkCYa6HjDBlB8mBd4Hi0gxB0_jn_ncQfIEDgNvTT2bvWIOiqGnWFYmWTfHBfAHj88ZcSWRAR15dMy1awFLoI8ejVnBLiTxpSOBYAUm48OunyTwDHxy4F6Ic3t1trGB6AWspm3zy0xtl9BIguugZKoknJqD4GOvXgemN5dsHfTMLWWvpCnqdbAWn0Zq-oRcvIs86NWqYdeBks125x4au7oxJ-Y8Kd1LRnkt1QQOGnvefP4DYGz8cA?type=png)
![Coding](assets/coding.png)

#### Dev Roadmap:

- [x] jwt auth
  - [x] pydantic + sqlalchemy
  - [x] /login endpoint
  - [x] client logic + ui
  - [x] refresh & retry logic
    - [x] debug why this isn't working
      - strategy was garbage, use superagent `.retry()` callback wiring
- [x] upload sound 
- [x] delete sound
- [x] edit/update sound
- [x] comment on sound at a certain timestamp like soundcloud
- audio controls
  - [x] firefox
  - [ ] chrome
- [x] write comments on sound
- [x] delete comments on sound
- [x] trim files if they're over 21 seconds using [pydub](https://github.com/jiaaro/pydub)
  - [wip] playwright test
- [x] remove superfluous password from auth system -> makes for a better auth/app experience
  - [x] migration: remove `hashed_password` column from db
  - [x] debug/fix avoid duplicate entries -> thank you postgres
- [ ] test patch/updateusername endpoint
- [x] deploy python fastapi to aws ec2
  - [x] caddy + nip.io for https/gateway
- [x] vercel deployment for client application
- [x] better file organization for logic
- [x] use enum strategy for simpler client route nav
- [x] changing url on same component refreshes component data accordingly
- [x] better ui for list view and detail page
  - [x] "more from this author"
- [x] search in header
- [x] better mobile experience
- [x] login flow ui test on firefox/webkit/chromium with playwright
